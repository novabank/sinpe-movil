﻿using System;
using System.IO;
using Novacompcr.SinpeMovil.Services;
using Novacompcr.SinpeMovil.ViewModels;
using Novacompcr.SinpeMovil.Views;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using SQLite;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil
{
    public partial class App : PrismApplication
    {
        public App() : this(null)
        {
        }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync("/NavigationPage/MainPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage>();
            containerRegistry.RegisterForNavigation<SendPage, SendViewModel>();
            containerRegistry.RegisterForNavigation<ConsultPage, ConsultViewModel>();
            containerRegistry.RegisterForNavigation<ShowContactsPage, ShowContactsViewModel>();
            containerRegistry.RegisterForNavigation<ShowFavoriteContactsPage, ShowFavoriteContactsViewModel>();
            containerRegistry.Register<ISMSService, SMSService>();

            DependencyService.RegisterSingleton<IDataBase>(new DataBase()
            {
                Connection = new SQLiteAsyncConnection(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "SQLite.db3"))
            });
            DependencyService.Register<IResourse, Resourse>();
        }
    }

    public class Resourse : IResourse
    {
        public Page GetPage()
        {
            return App.Current.MainPage;
        }

        public object GetResource(string key)
        {
            return App.Current.Resources[key];
        }
    }
}
