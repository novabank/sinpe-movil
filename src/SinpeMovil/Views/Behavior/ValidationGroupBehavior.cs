﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.Views
{
    public class ValidationGroupBehavior : Behavior<View>
    {
        IList<ValidationBehavior> _validationBehaviors;
        public static readonly BindableProperty IsValidProperty =
            BindableProperty.Create(nameof(IsValid),
                                    typeof(bool),
                                    typeof(ValidationGroupBehavior),
                                    false);

        public ValidationGroupBehavior()
        {
            _validationBehaviors = new List<ValidationBehavior>();
        }

        public void Add(ValidationBehavior validationBehavior)
        {
            _validationBehaviors.Add(validationBehavior);
        }

        public void Remove(ValidationBehavior validationBehavior)
        {
            _validationBehaviors.Remove(validationBehavior);
        }

        public void Validate()
        {
            bool isValid = true;

            foreach (ValidationBehavior validationItem in _validationBehaviors)
            {
                isValid = isValid && validationItem.IsValid;
            }

            IsValid = isValid;
        }

        public bool IsValid
        {
            get => (bool)GetValue(IsValidProperty);
            set => SetValue(IsValidProperty, value);
        }
    }
}
