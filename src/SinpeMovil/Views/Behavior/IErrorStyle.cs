﻿using System;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.Views
{
    public interface IErrorStyle
    {
        void ShowError(View view, string message);
        void RemoveError(View view);
    }
}
