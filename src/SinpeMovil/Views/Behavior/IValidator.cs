﻿using System;
namespace Novacompcr.SinpeMovil.Views
{
    public interface IValidator
    {
        string Message { get; set; }
        bool Check(string value);
    }
}
