﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.Views
{
    public class ValidationBehavior : Behavior<View>
    {
        IErrorStyle _style = new BasicErrorStyle();
        public bool IsValid = false;
        View _view;
        public string PropertyName { get; set; }
        public string DefaultValue { get; set; }
        public ValidationGroupBehavior Group { get; set; }
        public ObservableCollection<IValidator> Validators { get; set; } = new ObservableCollection<IValidator>();

        public bool Validate()
        {
            bool isValid = true;
            string errorMessage = "";
            string value = _view.GetType()
                            .GetProperty(PropertyName)
                            .GetValue(_view) as string;

            if (value != DefaultValue)
                foreach (IValidator validator in Validators)
                {
                    bool result = validator.Check(value);
                    isValid = isValid && result;

                    if (!result)
                    {
                        errorMessage = validator.Message;
                        break;
                    }
                }

            if(IsValid != isValid)
            {
                IsValid = isValid;
                if (Group != null)
                {
                    Group.Validate();
                }
            }

            if (!isValid)
            {
                _style.ShowError(_view, errorMessage);

                return false;
            }
            else
            {
                _style.RemoveError(_view);

                return true;
            }
        }

        protected override void OnAttachedTo(BindableObject bindable)
        {
            base.OnAttachedTo(bindable);

            _view = bindable as View;
            _view.PropertyChanged += OnPropertyChanged;
            _view.Unfocused += OnUnFocused;

            if (Group != null)
            {
                Group.Add(this);
            }
        }

        protected override void OnDetachingFrom(BindableObject bindable)
        {
            base.OnDetachingFrom(bindable);

            _view.PropertyChanged -= OnPropertyChanged;
            _view.Unfocused -= OnUnFocused;

            if (Group != null)
            {
                Group.Remove(this);
            }
        }

        void OnUnFocused(object sender, FocusEventArgs e)
        {
            Validate();
        }

        void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PropertyName)
            {
                Validate();
            }
        }
    }
}