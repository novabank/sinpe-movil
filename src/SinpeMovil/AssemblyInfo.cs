using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

[assembly: ExportFont("Bold.otf", Alias = "bold")]
[assembly: ExportFont("Light.otf", Alias = "light")]
[assembly: ExportFont("FA5Regular.otf", Alias = "FontAwesome")]