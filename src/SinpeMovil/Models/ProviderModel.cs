﻿namespace Novacompcr.SinpeMovil.Models
{
    public class ProviderModel
    {
        public string Telefono { get; set; }

        public string Proveedor { get; set; } 
    }
}
