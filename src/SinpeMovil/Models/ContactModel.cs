﻿using System;
namespace Novacompcr.SinpeMovil.Models
{
    public class ContactModel
    {
        public string Name { get; set; }

        public string Number { get; set; }
    }
}
