﻿using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.Services
{
    public interface IResourse
    {
        object GetResource(string key);

        Page GetPage();
    }
}
