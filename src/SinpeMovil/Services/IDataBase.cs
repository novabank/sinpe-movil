﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Novacompcr.SinpeMovil.Services
{
    public interface IDataBase
    {
         SQLiteAsyncConnection Connection { get; set; }
    }
}
