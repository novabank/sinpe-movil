﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.Services
{
    public abstract class Service
    {
        private static string BASE_URI = DependencyService.Get<IResourse>().GetResource("URLBase").ToString();

        public async Task<T> GetAsync<T>(string uri, Dictionary<string, string> parameters = null)
        {
            StringBuilder paramsBuilder = new StringBuilder();
            if (parameters != null)
            {
                foreach (KeyValuePair<string, string> param in parameters)
                {
                    if (paramsBuilder.Length > 0)
                    {
                        paramsBuilder.Append("&");
                    }
                    paramsBuilder.Append($"{param.Key}={param.Value}");
                }
            }

            UriBuilder builder = new UriBuilder(new Uri($"{BASE_URI}{uri}"));
            builder.Query = paramsBuilder.ToString();

            using (HttpClient client = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(Convert.ToInt32(DependencyService.Get<IResourse>().GetResource("TimeOut")))
            })
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add(DependencyService.Get<IResourse>().GetResource("Auth").ToString(), DependencyService.Get<IResourse>().GetResource("Token").ToString());

                HttpResponseMessage response = await client.GetAsync(builder.Uri).ConfigureAwait(false);
                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.InternalServerError:
                        throw new ServiceException("Exception from api");
                    case System.Net.HttpStatusCode.BadRequest:
                        throw new ServiceException("Exception from api");
                    case System.Net.HttpStatusCode.Conflict:
                        throw new ServiceException("Exception from api");
                    case System.Net.HttpStatusCode.Unauthorized:
                        throw new ServiceException("Exception from api");
                }

                string ans = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(ans);
            }
        }
    }
}
