﻿using SQLite;

namespace Novacompcr.SinpeMovil.Services
{
    public class DataBase : IDataBase
    {
        public SQLiteAsyncConnection Connection { get; set; }
    }
}
