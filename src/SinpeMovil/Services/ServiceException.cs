﻿using System;

namespace Novacompcr.SinpeMovil.Services
{
    public class ServiceException : Exception
    {
        public ServiceException(string message) : base(message)
        {
        }
    }
}
