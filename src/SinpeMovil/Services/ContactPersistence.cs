﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Novacompcr.SinpeMovil.Models;
using SQLite;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.Services
{
    public class ContactPersistence
    {
        private SQLiteAsyncConnection Connection { get; set; }

        public ContactPersistence()
        {
            Connection = DependencyService.Get<IDataBase>().Connection;
            Connection.CreateTableAsync<ContactModel>().Wait();
        }

        public Task<List<ContactModel>> GetItemsAsync()
        {
            return Connection.Table<ContactModel>().ToListAsync();
        }

        public Task<ContactModel> GetAsync(string number)
        {
            return Connection.Table<ContactModel>().FirstOrDefaultAsync(i => i.Number == number);
        }

        public Task<int> Save(ContactModel item)
        {
            return Connection.InsertAsync(item);
        }
    }
}