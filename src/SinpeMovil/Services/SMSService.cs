﻿using Novacompcr.SinpeMovil.Models;
using System.Threading.Tasks;

namespace Novacompcr.SinpeMovil.Services
{
    public class SMSService : Service, ISMSService
    {
        public async Task<ProviderModel> GetActiveLine() => await GetAsync<ProviderModel>("linea-activa");

        public async Task<ClientModel> GetInfoAccount(string numTelefono) => await GetAsync<ClientModel>($"info-telefono/{numTelefono}");

    }
}
