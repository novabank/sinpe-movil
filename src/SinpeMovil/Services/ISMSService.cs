﻿using Novacompcr.SinpeMovil.Models;
using System.Threading.Tasks;

namespace Novacompcr.SinpeMovil.Services
{
    public interface ISMSService
    {
        Task<ProviderModel> GetActiveLine();

        Task<ClientModel> GetInfoAccount(string numTelefono);
    }
}
