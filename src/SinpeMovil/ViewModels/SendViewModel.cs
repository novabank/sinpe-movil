﻿using Novacompcr.SinpeMovil.Models;
using Novacompcr.SinpeMovil.Services;
using Novacompcr.SinpeMovil.Views;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.ViewModels
{
    public class SendViewModel : BaseViewModel
    {
        #region Fields
        private ContactPersistence _contactPersistance;
        private ISMSService _sMSService;

        #endregion
        #region Instances
        private ContactModel _contact;
        public ContactModel Contact
        {
            get => _contact;
            set => SetProperty(ref _contact, value);
        }

        private string _number;
        public string Number
        {
            get => _number;
            set => SetProperty(ref _number, value);
        }

        private decimal? _amount;
        public decimal? Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private string _description;
        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }
        #endregion
        #region Commands

        public ICommand GoToFavoriteCommand { get; private set; }

        public ICommand GoToContactCommand { get; private set; }

        public ICommand NumberChangedCommand { get; private set; }

        public ICommand SendCommand { get; private set; }

        public ICommand NumberUnfocusedCommand { get; private set; }
        #endregion

        private async System.Threading.Tasks.Task<string> GetNumberAsync()
        {
            try
            {
                IsBusy = true;
                var task = await _sMSService.GetActiveLine();
                IsBusy = false;
                return task.Telefono;
            }
            catch (Exception)
            {
                IsBusy = false;
                return DependencyService.Get<IResourse>().GetResource("Number").ToString();
            }
        }

        private async System.Threading.Tasks.Task<string> GetNameAsync()
        {
            try
            {
                IsBusy = true;
                var task = await _sMSService.GetInfoAccount(Number.Replace("-", ""));
                IsBusy = false;
                return task.NomCliente;
            }
            catch (Exception)
            {
                IsBusy = false;
                return null;
            }
        }

        public SendViewModel(INavigationService navigationService, ISMSService sMSService) : base(navigationService)
        {
            GoToFavoriteCommand = new DelegateCommand(GoToFavorite);
            GoToContactCommand = new DelegateCommand(GoToContact);
            NumberChangedCommand = new DelegateCommand(NumberChanged);
            SendCommand = new DelegateCommand(Send);
            NumberUnfocusedCommand = new DelegateCommand(NumberUnfocusedAsync);

            _sMSService = sMSService;
            _contactPersistance = new ContactPersistence();
        }

        private async void NumberUnfocusedAsync()
        {
            if (!string.IsNullOrEmpty(Number) &&
                Regex.IsMatch(Number, "^[0-9]{4}-[0-9]{4}$") &&
                Contact == null)
            {
                string name = await GetNameAsync();

                if (!string.IsNullOrEmpty(name))
                    Contact = new ContactModel()
                    {
                        Name = name,
                        Number = Number.Replace("-", "")
                    };
            }
        }

        private async void Send()
        {
            try
            {
                var message = new SmsMessage($"PASE {Amount.Value} {Number.Replace("-", "")} {Description}", new[] { await GetNumberAsync() });
                await Sms.ComposeAsync(message);

                Contact = null;
                Amount = null;
                Description = null;
                Number = null;
            }
            catch (FeatureNotSupportedException)
            {
                // Sms is not supported on this device.
            }
        }

        private void NumberChanged()
        {
            Contact = null;
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            string number = string.Empty;
            if (parameters.ContainsKey(nameof(ShowContactsViewModel)))
            {
                var contact = parameters.GetValue<ContactModel>(nameof(ShowContactsViewModel));
                number = contact.Number.Replace(" ", "-");

                if (number.Length == 8)
                {
                    number = number.Insert(4, "-");
                }
                Number = number;
                Contact = new ContactModel()
                {
                    Name = contact.Name,
                    Number = contact.Number
                };

                var favorito = await _contactPersistance.GetAsync(Contact.Number);
                if (favorito == null && await DependencyService.Get<IResourse>().GetPage().DisplayAlert("¿Agregar como favorito?", "¿Desea agregar este contacto como Favorito?", "Si", "No"))
                {
                    await _contactPersistance.Save(Contact);
                }
            }

            if (parameters.ContainsKey(nameof(ShowFavoriteContactsViewModel)))
            {
                number = parameters.GetValue<ContactModel>(nameof(ShowFavoriteContactsViewModel)).Number.Trim();
                number = number.Replace(" ", "-").Replace("+506", "");

                if (number.Length == 8)
                {
                    number = number.Insert(4, "-");
                }
                Number = number;

                Contact = parameters.GetValue<ContactModel>(nameof(ShowFavoriteContactsViewModel));
            }
        }

        #region Private Methods
        private void GoToFavorite()
        {
            NavigationService.NavigateAsync(nameof(ShowFavoriteContactsPage));
        }

        private async void GoToContact()
        {
            var status = await Permissions.CheckStatusAsync<Permissions.ContactsRead>();

            if (status == PermissionStatus.Denied && Device.iOS == Device.RuntimePlatform)
            {
                await DependencyService.Get<IResourse>().GetPage().DisplayAlert("Advertencia", "Debe habilitar el permiso de lectura de contactos", "Aceptar");
                return;
            }

            if (status != PermissionStatus.Granted)
            {
                status = await Permissions.RequestAsync<Permissions.ContactsRead>();
            }

            if (status == PermissionStatus.Granted)
            {
                await NavigationService.NavigateAsync(nameof(ShowContactsPage));
            }
        }
        #endregion
    }
}
