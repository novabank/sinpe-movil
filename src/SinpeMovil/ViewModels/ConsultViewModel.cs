﻿using Novacompcr.SinpeMovil.Services;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Novacompcr.SinpeMovil.ViewModels
{
    public class ConsultViewModel : BaseViewModel
    {
        #region Fields
        private ISMSService _sMSService;
        #endregion

        #region Commands

        public ICommand ConsultLastMovCommand { get; private set; }

        public ICommand ConsutlBalanceCommand { get; private set; }

        public ICommand InactiveCommand { get; private set; }

        public ICommand GoToSiteCommand { get; private set; }


        #endregion

        #region Public Propeties
        public string Version => VersionTracking.CurrentVersion;
        #endregion

        private async System.Threading.Tasks.Task<string> GetNumberAsync()
        {
            try
            {
                IsBusy = true;
                var task = await _sMSService.GetActiveLine();
                IsBusy = false;
                return task.Telefono;
            }
            catch (Exception)
            {
                IsBusy = false;
                return DependencyService.Get<IResourse>().GetResource("Number").ToString();
            }
        }

        public ConsultViewModel(INavigationService navigationService, ISMSService sMSService) : base(navigationService)
        {
            _sMSService = sMSService;
            ConsultLastMovCommand = new DelegateCommand(async () =>
            {
                try
                {
                    var message = new SmsMessage($"ULTIMOS", new[] { await GetNumberAsync() });
                    await Sms.ComposeAsync(message);
                }
                catch (FeatureNotSupportedException)
                {
                    // Sms is not supported on this device.
                }
            });
            InactiveCommand = new DelegateCommand(async () =>
            {
                try
                {
                    var message = new SmsMessage($"INACTIVE", new[] { await GetNumberAsync() });
                    await Sms.ComposeAsync(message);
                }
                catch (FeatureNotSupportedException)
                {
                    // Sms is not supported on this device.
                }
            });
            ConsutlBalanceCommand = new DelegateCommand(async () =>
            {
                try
                {
                    var message = new SmsMessage($"SALDO", new[] { await GetNumberAsync() });
                    await Sms.ComposeAsync(message);
                }
                catch (FeatureNotSupportedException)
                {
                    // Sms is not supported on this device.
                }
            });
            GoToSiteCommand = new DelegateCommand(() =>
            {
                try
                {
                    Browser.OpenAsync(new Uri("https://www.crnova.com/"), BrowserLaunchMode.SystemPreferred);
                }
                catch (Exception)
                {
                }
            });
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
        }
    }
}
