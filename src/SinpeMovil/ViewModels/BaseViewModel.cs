﻿using Prism.Mvvm;
using Prism.Navigation;

namespace Novacompcr.SinpeMovil.ViewModels
{
    public abstract class BaseViewModel : BindableBase, INavigationAware
    {
        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        protected INavigationService NavigationService { get; private set; }

        public abstract void OnNavigatedFrom(INavigationParameters parameters);
        public abstract void OnNavigatedTo(INavigationParameters parameters);

        public BaseViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }
    }
}
