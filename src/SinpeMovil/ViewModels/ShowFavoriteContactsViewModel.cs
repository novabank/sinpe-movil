﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Novacompcr.SinpeMovil.Models;
using Novacompcr.SinpeMovil.Services;
using Prism.Commands;
using Prism.Navigation;

namespace Novacompcr.SinpeMovil.ViewModels
{
    public class ShowFavoriteContactsViewModel:BaseViewModel
    {
        #region Fields
        private ContactPersistence _contactPersistance;
        #endregion

        private ObservableCollection<ContactModel> _contacts;
        public ObservableCollection<ContactModel> Contacts
        {
            get => _contacts;
            set => SetProperty(ref _contacts, value);
        }

        public ICommand SelectContactCommand { get; private set; }

        public ShowFavoriteContactsViewModel(INavigationService navigationService) : base(navigationService)
        {
            _contactPersistance = new ContactPersistence();
            InitializeClass();
            SelectContactCommand = new DelegateCommand<ContactModel>(SelectContact);
        }

        private async void InitializeClass()
        {
            IsBusy = true;

            Contacts = new ObservableCollection<ContactModel>((await _contactPersistance.GetItemsAsync()).OrderBy(x => x.Name));

            IsBusy = false;
        }


        private void SelectContact(ContactModel contact)
        {
            NavigationParameters parameters = new NavigationParameters();
            parameters.Add(nameof(ShowFavoriteContactsViewModel), contact);
            NavigationService.GoBackAsync(parameters);
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
        }
    }
}
