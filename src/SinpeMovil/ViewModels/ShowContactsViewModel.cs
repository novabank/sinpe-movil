﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Novacompcr.SinpeMovil.Models;
using Prism.Commands;
using Prism.Navigation;

namespace Novacompcr.SinpeMovil.ViewModels
{
    public class ShowContactsViewModel : BaseViewModel
    {
        private static List<ContactModel> _contacts;
        private List<ContactModel> _filterContacts;
        public List<ContactModel> FilterContacts
        {
            get => _filterContacts;
            set => SetProperty(ref _filterContacts, value);
        }

        private string _search;
        public string Search
        {
            get => _search;
            set => SetProperty(ref _search, value);
        }

        public ICommand SelectContactCommand { get; private set; }

        public ICommand SearchContactsCommand { get; private set; }

        public ShowContactsViewModel(INavigationService navigationService) : base(navigationService)
        {
            InitializeClass();
            SelectContactCommand = new DelegateCommand<ContactModel>(SelectContact);
            SearchContactsCommand = new DelegateCommand(SearchContacts);
        }

        private void SearchContacts()
        {
            if (string.IsNullOrEmpty(Search))
            {
                FilterContacts = _contacts;
            }
            else
            {
                FilterContacts = _contacts.Where(x=> x.Name.IndexOf(Search, StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();
            }
        }

        private async void InitializeClass()
        {
            if (_contacts == null)
            {
                IsBusy = true;

                await Task.Run(() =>
                {
                    var contacts = Plugin.ContactService.CrossContactService.Current.GetContactListAsync().Result;
                    var contactModels = contacts.Select(contact =>
                    {
                        var contactModel = new ContactModel()
                        {
                            Name = contact.Name,
                            Number = string.IsNullOrEmpty(contact.Number) ? contact.Number : contact.Number.Replace("+506", "").Trim()
                        };

                        return contactModel;
                    });

                    _contacts = contactModels.Where(x => !string.IsNullOrEmpty(x.Number) &&
                                (x.Number.StartsWith("8") || x.Number.StartsWith("7") ||
                                x.Number.StartsWith("6") || x.Number.StartsWith("3")))
                        .OrderBy(x => x.Name).ToList();
                });

                IsBusy = false;
            }

            FilterContacts = _contacts;
        }


        private void SelectContact(ContactModel contact)
        {
            NavigationParameters parameters = new NavigationParameters();
            parameters.Add(nameof(ShowContactsViewModel), contact);
            NavigationService.GoBackAsync(parameters);
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
        }
    }
}
